# jdbc-log-agent

#### 介绍
一个 **javaagent** 插件: 基于 **JDBC** 的 **SQL+参数+完整SQL** 日志输出.  
支持的数据库: `MySQL`, `Oracle`, `PostgreSQL`.

#### 使用
下载或构建出jar包(如: `jdbc-log-agent.jar`), 然后在vm参数加上  
`-javaagent:"路径/jdbc-log-agent.jar"` (jar包路径无空格,可省略"")   
就可以在执行SQL的时候就会输出参数.

#### 示例
1. jvm参数配置  
   ![vm配置](https://images.gitee.com/uploads/images/2020/1109/120606_26139a5b_1548105.png)

2. 结果:打印SQL+参数  
   ![结果](https://images.gitee.com/uploads/images/2021/0118/113914_a215f555_1548105.png)

#### 参数配置
| 参数名 | 默认值 | 解释 |
|---------|-------|------|
| debug   | false | 开启debug模式，打印代理的异常 |
| exclude |       | 排除SQL，不打印  |  

例：-javaagent:"D:/jdbc-log-agent.jar"=`"?exclude=(?i)SELECT 1(?: FROM dual)?"`  
