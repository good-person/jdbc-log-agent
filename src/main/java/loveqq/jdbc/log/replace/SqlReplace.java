package loveqq.jdbc.log.replace;

import java.util.Map;

/**
 * SQL替换动态参数(占位符: ?)通用接口
 */
public interface SqlReplace {

    /**
     * SQL替换动态参数(占位符: ?)
     *
     * @param sql      原生SQL
     * @param paramMap SQL<参数位置, 参数值>
     * @param dbType   数据库类型(jdbc:数据库类型:...)
     *
     * @return 替换好的SQL
     */
    String replace(String sql, Map<Integer, Object> paramMap, String dbType) throws Exception;
}
