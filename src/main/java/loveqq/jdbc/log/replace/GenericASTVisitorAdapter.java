package loveqq.jdbc.log.replace;

import com.alibaba.druid.sql.dialect.db2.visitor.DB2ASTVisitor;
import com.alibaba.druid.sql.dialect.h2.visitor.H2ASTVisitor;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlASTVisitor;
import com.alibaba.druid.sql.dialect.oracle.visitor.OracleASTVisitor;
import com.alibaba.druid.sql.dialect.postgresql.visitor.PGASTVisitor;
import com.alibaba.druid.sql.dialect.sqlserver.visitor.SQLServerASTVisitor;
import com.alibaba.druid.sql.visitor.SQLASTVisitor;

/**
 * 通用数据库AST节点遍历适配器
 * DB2, MySQL, SQLServer, H2, Oracle, Postgresql 数据库可用
 */
public class GenericASTVisitorAdapter implements SQLASTVisitor,
        DB2ASTVisitor, MySqlASTVisitor, SQLServerASTVisitor, H2ASTVisitor, OracleASTVisitor, PGASTVisitor {
}
